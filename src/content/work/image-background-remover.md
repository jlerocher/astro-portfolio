---
title: Blog DevOps Dynasty
publishDate: 2023-09-08 18:26:00
img: /assets/dd1.png
img_alt: Devops Dynasty blog
description: |
  I developed this blog where you can login, create posts, read posts and other actions ...
tags:
  - Supabase
  - NextJS
  - MongoDB
  - Postgres
  - Typescript
  - Gitlab
---
![New post interface](../../../public/assets/dd2.png)

- You can find hosted project here <https://nextjs-devops-dynasty-git-main-jlerochers-projects.vercel.app/>

- Source code here <https://gitlab.com/jlerocher/nextjs-devops-dynasty>
